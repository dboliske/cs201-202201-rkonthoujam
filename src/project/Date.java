package project;

public class Date {
	private int day;
	private int month;
	private int year;
	
	// default constructor
	public Date() {
		this.day = 1;
		this.month = 1;
		this.year = 2000;
	}
	
	// non-default construct
	public Date(int day, int month, int year) {
		// valid
		if (this.validDate(day, month, year)) {
			this.day = day;
			this.month = month;
			this.year = year;
		} else {
			System.out.println("Please enter a valid date, in the format of dd/mm/yyyy \n");
		}
	}

	// Public method to check whether input date is valid
	public boolean validDate (int day, int month, int year) {
		// Handle month before everything else - cannot be greater than 12
		if (month > 12) {
			return false;
		}
		// Handle date - cannot be less than 1
		if (day < 1) {
			return false;
		}
		// Handling Leap Year
		if ((year % 4 == 0) || (year % 1000 == 0)) {
			if ((month == 2) && (day > 29)) {
				return false;
			}
		}
		// Handling June
		if (month == 6) {
			if (day > 31) {
				return false;
			}
		}
		// Handling Regular Cases
		if (month % 2 == 0) {
			if (day > 30) {
				return false;
			}
		} else {
			if (day > 31) {
				return false;
			}
		}
		return true;
	}
	
	// accessor method to access the day of the date
	public int getDay() {
		return this.day;
	}
	
	// accessor method to access the month of the date
	public int getMonth() {
		return this.month;
	}
	
	// accessor method to access the year of the date
	public int getYear() {
		return this.year;
	}
	
	// modifier method to access the day of the date
	public void setDay(int day) {
		this.day = day;
	}
	
	// modifier method to access the month of the date
	public void setMonth(int month) {
		this.month = month;
	}
	
	// modifier method to access the year of the date
	public void setYear(int year) {
		this.year = year;
	}
	
	// Public method to represent Date object in a user-friendly string format
	public String toString() {
		return Integer.toString(this.getDay()) + '/' + Integer.toString(this.getMonth()) + '/' + Integer.toString(this.getYear());
	}
	
	// Overriding default equals() to compare whether two Date instances are equal
	public boolean equals(Date other) {
		if (this.day != other.day) {
			return false;
		}
		if (this.month != other.month) {
			return false;
		}
		if (this.year != other.year) {
			return false;
		}
		return true;
	}
}