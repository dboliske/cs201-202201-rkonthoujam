package project;

//Child class to represent AgeRestricted, one of the three variety among all items in stock
public class AgeRestricted extends Item {
	public int ageRestriction;
	
	// default constructor
	public AgeRestricted() {
		super();
		this.ageRestriction = 0;
	}
	
	// non-default constructor
	public AgeRestricted(String name, double price, int ageRestriction) {
		super(name, price);
		this.ageRestriction = ageRestriction;
	}
	
	// accessor method to access the age restriction of the item
	public int getAgeRestriction() {
		return this.ageRestriction;
	}
	
	// modifier method to modify the age restriction of the item
	public void setAgeRestriction(int ageRestriction) {
		this.ageRestriction = ageRestriction;
	}
	
	// overriding default toString() to represent name, price, and age restriction of the item
	public String toString() {
		return this.name + ',' + Double.toString(this.price) + ',' + Integer.toString(this.ageRestriction);
	}
	
	// overriding default equals() to compare whether two AgeRestricted instances are equal
	public boolean equals(AgeRestricted other) {
		if (!this.name.equals(other.name)) {
			return false;
		} 
		if (this.price != other.price) {
			return false;
		}
		if (this.ageRestriction != other.ageRestriction) {
			return false;
		}
		return true;
	}
}
