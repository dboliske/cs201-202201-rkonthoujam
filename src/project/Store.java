package project;

import java.io.*;
import java.util.*;

public class Store {
	// Create ArrayList to hold all items in stock
	private static ArrayList<ArrayList<Item>> stock = new ArrayList<ArrayList<Item>>();
	
	static ArrayList<ArrayList<Item>> readExistingFile() {
		// Create three ArrayLists to inside stock ArrayList, one ArrayList for each type of item
		for (int i = 0; i < 3; i++) {
			stock.add(new ArrayList<Item>());
		}
		
		try {
			// Read CSV file into File object file
			File file = new File("./src/project/stock.csv");
			Scanner input = new Scanner(file);
			
			// Consume the first line since the first row is made of headers
			input.nextLine();
			
			while (input.hasNextLine()) {
				// To hold the current row
				String line = input.nextLine();
				// To hold values in different columns of the current row
				String[] data = line.split(",");
				
				try {
					// If the third column is 10 characters, meaning that it contains the expiration date, implying that it is an instance of the Produce class
					if (data[2].length() == 10) {
						// Splitting the third column into day, month, and year by using "/" as the delimiter
						String[] date = data[2].split("/");
						// Initializing Date instance using the day, month, and year as the arguments to the Date constructor
						Date exp = new Date(Integer.parseInt(date[1]), Integer.parseInt(date[0]), Integer.parseInt(date[2]));
						// Initializing Produce instance
						Item item = new Produce(data[0], Double.parseDouble(data[1]), exp);
						// Append the instance to the 1st Sub ArrayList
						stock.get(0).add(item);
					}
					// If the third column is less than 3 characters, meaning that it contains the age restriction, implying that it is an instance of the AgeRestricted class
					else if (data[2].length() < 3) {
						// Initializing AgeRestricted instance
						Item item = new AgeRestricted(data[0], Double.parseDouble(data[1]), Integer.parseInt(data[2]));
						// Append the instance to the 3rd Sub ArrayList
						stock.get(2).add(item);
					}
				// If the third column is empty, implying that it is an instance of the Shelved class
				} catch (ArrayIndexOutOfBoundsException e) {
					// Initializing Shelved instance
					Item item = new Shelved(data[0], Double.parseDouble(data[1]));
					// Append the instance to the 2nd Sub ArrayList
					stock.get(1).add(item);
				}	
			}
			return stock;
			
		// Catch block to handle the case if the CSV data file was not found
		} catch (FileNotFoundException error) {
	        System.out.println("File Not Found. \n");
		        error.printStackTrace();
		}
		
		return null;
	}
	
	// Method to add Produce items to the stock 
	public static void addProduce() {
		Scanner input = new Scanner(System.in);
		
		// Prompt user to enter the item's name
		System.out.println("Please enter the item's name: ");
		String name = input.nextLine();
		
		// Prompt user to enter the item's price
		System.out.println("Please enter the item's price: ");
		double price = input.nextDouble();
		
		input.nextLine();
		
		// Prompt user to enter the item's expiration date
		System.out.println("Please enter the item's expiration date, in the format of dd/mm/yyyy: ");
		String expirationDate = input.nextLine();
		// Splitting the input expiration date into day, month, and year by using "/" as the delimiter
		String[] date = expirationDate.split("/");
		
		if (date.length != 3) {
			System.out.println("Please enter a valid date, in the format of dd/mm/yyyy \n");
			return;
		}
		
		// Initializing Date instance using the day, month, and year as the arguments to the Date constructor
		Date exp = new Date(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]));
		
		// Initializing Produce instance
		Item item = new Produce(name, price, exp);
		
		// Append the instance to the 1st Sub ArrayList
		Store.stock.get(0).add(item);
	}
	
	// Method to add Shelved items to the stock
	public static void addShelved() {
		Scanner input = new Scanner(System.in);
		
		// Prompt user to enter the item's name
		System.out.println("Please enter the item's name: ");
		String name = input.nextLine();
		
		// Prompt user to enter the item's price
		System.out.println("Please enter the item's price: ");
		double price = input.nextDouble();
		
		input.nextLine();
		
		// Initializing Shelved instance
		Item item = new Shelved(name, price);
		
		// Append the instance to the 2nd Sub ArrayList
		Store.stock.get(1).add(item);
	}
	
	// Method to add Age Restricted items to the stock
	public static void addAgeRestricted() {
		Scanner input = new Scanner(System.in);
		
		// Prompt user to enter the item's name
		System.out.println("Please enter the item's name: ");
		String name = input.nextLine();
		
		
		// Prompt user to enter the item's price
		System.out.println("Please enter the item's price: ");
		double price = input.nextDouble();
		
		input.nextLine();

		// Prompt user to enter the item's age restriction
		System.out.println("Please enter the item's age restriction (integer): ");
		int ageRestriction = input.nextInt();
		
		// Initializing AgeRestricted instance
		Item item = new AgeRestricted(name, price, ageRestriction);
		
		// Append the instance to the 3rd Sub ArrayList
		Store.stock.get(2).add(item);
	}
	
	// Method to add items in stock to cart and sell items
	public static ArrayList<Item> sellItem() {
		// Initializing ArrayList cart to hold the items before selling them
		ArrayList<Item> cart = new ArrayList<Item>();
		
		Scanner input = new Scanner(System.in);
		
		// Prompt user to enter the item's name
		System.out.println("Please enter the item's name: ");
		String name = input.nextLine();
		
		// Prompt user to enter the item's quantity to sell
		System.out.println("Please enter the quantity sold (integer): ");
		int quantity = input.nextInt();
		
		input.nextLine();
		
		// Use loop to sell one item (with the specified name) at a time until the quantity is met
		// Iterate through all Sub ArrayList
		for (int i = 0; i < Store.stock.size(); i++) {
			// Iterate through all items in each Sub ArrayList
			for (int j = 0; j < Store.stock.get(i).size(); j++) {
				if (quantity == 0) {
					return cart;
				}
				// Check if the current item's name matches the item's name input by user to search for
				if (Store.stock.get(i).get(j).getName().equals(name)) {
					// Add the current item to cart
					cart.add(Store.stock.get(i).get(j));	
					
					// Remove the current item from ArrayList stock
					Store.stock.get(i).remove(j);
					
					// To adjust the index after removing item
					j -= 1;
					
					// Decrement the quantity to sell every loop
					quantity -= 1;
				}
			}
		}
		
		// Handle the case when item is not found or not enough items in stock
		if (quantity != 0) {
			System.out.println("Not enough inventory in stock. \n");
			return cart;
		}
		
		// Return the ArrayList cart which is holding all the sold items
		return cart;
	}
	
	// Method to search items in stock using the item's name
	public static ArrayList<Item> searchItem() {
		// Initializing an ArrayList results to hold the search results
		ArrayList<Item> results = new ArrayList<Item>();
		
		Scanner input = new Scanner(System.in);
		
		// Prompt user to enter the item's name
		System.out.println("Please enter the item's name: ");
		String name = input.nextLine();
		
		// Iterate through all Sub ArrayList
		for (int i = 0; i < Store.stock.size(); i++) {
			// Iterate through all items in each Sub ArrayList
			for (int j = 0; j < Store.stock.get(i).size(); j++) {
				// Check if the current item's name matches the item's name input by user to search for
				if (Store.stock.get(i).get(j).getName().equals(name)) {
					// If they matches, add the current item to ArrayList results
					results.add(Store.stock.get(i).get(j));
				}
			}
		}
		
		System.out.println(results);
		return results;
	}
	
	// Method to search items in stock using the item's name and change their attributes (name or price)
	public static void modifyItem() {
		Scanner input = new Scanner(System.in);
		
		// Initializing string and int newName and newPrice
		String newName = "";
		int newPrice = -1;
		
		// Prompt user to enter the item's name to search
		System.out.println("Please enter the item's name you want to modify: ");
		String name = input.nextLine();
		
		// Prompt user to enter what to modify
		System.out.println("Please enter an option:\n 1. Modify items' name\n 2. Modify items' price");
		int option = input.nextInt();
		
		input.nextLine();

		
		if (option == 1) {
			// Prompt user to enter the new name
			System.out.println("What is the new name?");
			newName = input.nextLine();
			
		} else if (option == 2) {
			// Prompt user to enter the new price
			System.out.println("What is the new price?");
			newPrice = input.nextInt();
			
		}
		
		// Iterate through all Sub ArrayList
		for (int i = 0; i < Store.stock.size(); i++) {
			// Iterate through all items in each Sub ArrayList
			for (int j = 0; j < Store.stock.get(i).size(); j++) {
				// Check if the current item's name matches the item's name input by user to search for
				if (Store.stock.get(i).get(j).getName().equals(name)) {
					// If input is 1, modify name. Otherwise, modify price.
					if (option == 1) {
						// Set current item's name
						Store.stock.get(i).get(j).setName(newName);
					} else if (option == 2) {
						// Set current item's price
						Store.stock.get(i).get(j).setPrice(newPrice);
					}
				}
			}
		}		
	}
	
	// Method to save all current items to current_stock.csv
	public static void saveStock() {
		try {
			// Initialize FileWriter object to save file
			FileWriter writer = new FileWriter("./src/project/current_stock.csv");
			
			// Iterate through all Sub ArrayList
			for (int i = 0; i < Store.stock.size(); i++) {
				// Iterate through all items in each Sub ArrayList
				for (int j = 0; j < Store.stock.get(i).size(); j++) {
					// Write each item to the CSV, one line for each item
					writer.write(Store.stock.get(i).get(j).toString() + "\n");
				}
			}
			
			// Close writer
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Main method to continuously prompt user options to use the program
	public static void main(String[] args) {
		// Call readExistingFile() to read the data CSV file and add items to the ArrayList
		// Assign the return value, the ArrayList filled with items, to Store.stock
		Store.stock = readExistingFile();
		while (true) {
			Scanner input = new Scanner(System.in);
			
			System.out.println("Please enter an option: \n 1. Add item to stock \n 2. Sell items from stock \n 3. Search items in stock \n 4. Modify items in stock \n 5. Save current stock to current_stock.csv \n 6. Exit the program ");
			
			try {
				// Allows users to input how they want to use this program
				int option = input.nextInt();
				// If the user input 1, proceed to prompt users with more options to select the kind of item they want to add to the stock
				if (option == 1) {
					System.out.println("Please enter a search option: \n 1. Add produce item to stock \n 2. Add shelved item to stock \n 3. Add age restricted item to stock: ");
					int item_type = input.nextInt();
					// If the user input 1, call addProduce() function o add produce items to the stock
					if (item_type == 1) {
						addProduce();
					// If the user input 2, call addShelved() function to add shelved items to the stock
					} else if (item_type == 2) {
						addShelved();
					// If the user input 3, call addAgeRestricted() function to add age restricted items to the stock
					} else if (item_type == 3) {
						addAgeRestricted();
					} else {
						System.out.println("Please enter a valid option (1 to 3)");
					}
				// If the user input 2, call the sellItem() function to first add the instances with the specified name to the cart, and sell them (delete them from the ArrayList)
				} else if (option == 2) {
					sellItem();
				// If the user input 3, call the searchItem() function to search for all instances with the specified name
				} else if (option == 3) {
					searchItem();
				// If the user input 4, call the modifyItem() function to modify the attributes of all instances with the specified name
				} else if (option == 4) {
					modifyItem();
				// If the user input 5, call the saveStock() function to save all current items to current_stock.csv
				} else if (option == 5) {
					saveStock();
				// If the user input 6, break out of the program
				} else if (option == 6) {
					break;
				} else {
					System.out.println("Please enter a valid option (1 to 6)");
				}
			// If user input something not number, handle the InputMismatchException
			} catch (InputMismatchException e) {
				System.out.println("Please enter the correct type of input\n");
				continue;
			}
		}
	}
}


