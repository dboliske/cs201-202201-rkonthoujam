package project;

//Abstract class to provide a general template for items in stock
public abstract class Item {
	
	protected String name;
	protected double price;
	
	// default constructor
	public Item() {
		this.name = "banana";
		this.price = 5.0;
	}
	
	// non-default constructor
	public Item(String name, double price) {
		this.name = name;
		this.price = price;
	}
	
	// accessor method to access the name of the item
	public String getName() {
		return this.name;
	}
	
	// accessor method to access the price of the item
	public double getPrice() {
		return this.price;
	}
	
	// modifier method to modify the name of the item
	public void setName(String name) {
		this.name = name;
	}
	
	// modifier method to modify the price of the item
	public void setPrice(double price) {
		this.price = price;
	}

	abstract public String toString();
}
