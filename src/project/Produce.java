package project;

// Child class to represent Produce, one of the three variety among all items in stock
public class Produce extends Item {
	private Date exp;
	
	// default constructor
	public Produce() {
		super();
		this.exp = new Date(1,1,2000);
	}
	
	// non-default construct
	public Produce(String name, double price, Date exp) {
		super(name, price);
		this.exp = exp;
	}
	
	// accessor method to access the expiration date of the item
	public Date getExp() {
		return this.exp;
	}
	
	// modifier method to modify the expiration date of the item
	public void setDate(Date exp) {
		this.exp = exp;
	}
	
	// overriding default toString() to represent name, price, and expiration date of the item
	public String toString() {
		return this.name + ',' + Double.toString(this.price) + ',' + this.exp.toString();
	}
	
	// overriding default equals() to compare whether two Produce instances are equal
	public boolean equals(Produce other) {
		if (!this.name.equals(other.name)) {
			return false;
		} 
		if (this.price != other.price) {
			return false;
		}
		if (this.exp != other.exp) {
			return false;
		}
		return true;
	}
}
