package project;

//Child class to represent Shelves, one of the three variety among all items in stock
public class Shelved extends Item {
	// default constructor
	public Shelved() {
		super();
	}
	
	// non-default constructor
	public Shelved(String name, double price) {
		super(name, price);
	}
	
	// overriding default toString() to represent name and price of the item
	public String toString() {
		return this.name + ',' + Double.toString(this.price);
	}
	
	// overriding default equals() to compare whether two Shelved instances are equal
	public boolean equals(Shelved other) {
		if (!this.name.equals(other.name)) {
			return false;
		} 
		if (this.price != other.price) {
			return false;
		}
		return true;
	}
}
