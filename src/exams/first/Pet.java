package exams.first;

public class Pet {
	
	private String name;
	
	private int age;
	
	public Pet() {
		
		setName("");
		
		setAge(0);
		
	}
	
	public Pet(String name, int age) {
		
		setName(name);
		
		setAge(age);
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public String toString() {
		
		return "Name: " + getName() + ", Age: " + getAge();
		
	}
	
	public boolean equals(Pet obj) {
		
		if(this.getName().equals(obj.getName()) && this.getAge() == obj.getAge()) {
			
			return true;
			
		}
		
		return false;
	}

}
