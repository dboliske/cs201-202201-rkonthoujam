# Midterm Exam

## Total

97/100

## Break Down

1. Data Types:                  20/20
    - Compiles:                 5/5
    - Input:                    5/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  20/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               5/5
4. Arrays:                      20/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  5/5
5. Objects:                     17/20
    - Variables:                5/5
    - Constructors:             5/5
    - Accessors and Mutators:   3/5
    - toString and equals:      4/5

## Comments

1. Good
2. Good
3. Good
4. Good
5. Good, but the mutator doesn't validate `age` and the `equals` method doesn't take an Object as a parameter.
