package exams.second;

import java.util.*;

public class Sorting {

	public static void main(String[] args) {
		
		String[] arr = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		
		for (int i = 0; i < arr.length-1; i++) {
			
			for(int j = i+1; j < arr.length; j++) {
				
				if(arr[j].compareTo(arr[i]) < 0) {
					
				      String temp = arr[i];
				      
				      arr[i] = arr[j];
				      
				      arr[j] = temp;
					
				}
				
			}
			
		}
		
		System.out.println(Arrays.toString(arr));

	}

}