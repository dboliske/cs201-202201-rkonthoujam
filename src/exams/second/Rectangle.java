package exams.second;

public class Rectangle extends Polygon {
	private double width;
	private double height;
	
	public Rectangle() {
		super();
		width = height = 1;
	}
	
	public void setWidth(double width) {
		if (width > 0)
			this.width = width;
	}
	
	public void setHeight(double height) {
		if (height > 0)
			this.height = height;
	}
	
	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}
	
	public String toString() {
		return "Rectangle of size " + height + "x" + width;
	}
	
	@Override
	public double area() {
		return height * width;
	}

	@Override
	public double perimeter() {
		return 2 * (height + width);
	}
}
