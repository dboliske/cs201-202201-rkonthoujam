package exams.second;

public class Testing {

	public static void main(String[] args) {
		ComputerLab lab = new ComputerLab();
		
		System.out.println(lab.toString());
		lab.setBuilding("Stuart Hall");
		Classroom classroom = new Classroom();
		classroom.setBuilding(lab.getBuilding());
		System.out.println(classroom.toString());
		
		Rectangle r = new Rectangle();
		r.setHeight(10);
		r.setWidth(5);
		System.out.println(r.area());
		System.out.println(r.perimeter());
		System.out.println(r.toString());
		Circle c = new Circle();
		System.out.println(c.area());
		System.out.println(c.perimeter());
		System.out.println(c.toString());
	}

}
