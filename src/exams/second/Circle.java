package exams.second;

public class Circle extends Polygon {
	private double radius;
	
	public Circle() {
		super();
		setRadius(1);
	}
	
	public void setRadius(double radius) {
		if (radius > 0)
			this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public String toString() {
		return "Circle of radius " + radius;
	}

	@Override
	public double area() {
		return Math.PI * radius * radius;
	}

	@Override
	public double perimeter() {
		return 2 * Math.PI * radius;
	}
}
