package exams.second;

import java.util.Scanner;

public class Searching {
	private static int min(int i, int j) {
		if (i < j)
			return i;
		return j;
	}
	
	public static int jumpSearch(double[] arr, double item, int curr, int step) {
		if (curr >= arr.length)
			return -1;
		
		if (arr[curr] > item)
			return -1;
		else if (arr[curr] == item)
			return curr + 1;
		else {
			int next = min(curr + step, arr.length - 1);
			if (next == curr)
				return -1;
			
			if (arr[next] <= item)
				return jumpSearch(arr, item, next, step);
			else
				return jumpSearch(arr, item, curr + 1, step);
		}
		
		
	}

	public static void main(String[] args) {
		double[] arr = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		Scanner input = new Scanner(System.in);
		System.out.println("What value do you want to search for?");
		System.out.print("> ");
		double item = Double.parseDouble(input.nextLine());
		
		int idx = jumpSearch(arr, item, 0, (int)Math.sqrt(arr.length));
		if (idx == -1)
			System.out.println("This value is not present in the array. (-1)");
		else
			System.out.println("Value " + item + " can be found at position " + idx + " in the array.");
	}

}
