package exams.second;

public abstract class Polygon {
	protected String name;
	
	public Polygon() {
		name = "polygon";
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public abstract double area();
	public abstract double perimeter();
}
