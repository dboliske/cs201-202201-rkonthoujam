package exams.second;

public class Classroom {
	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		building = "default";
		roomNumber = "0";
		seats = 20;
	}
	
	public void setBuilding(String building) {
		this.building = building;
	}
	
	public void setRoomNumber(String number) {
		this.roomNumber = number;
	}
	
	public void setSeats(int seats) {
		if (seats > 0)
			this.seats = seats;
	}

	public String getBuilding() {
		return building;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public int getSeats() {
		return seats;
	}
	
	public String toString() {
		return "Classroom located in the " + building + " building. Room number: " + roomNumber + ". The classroom has " + seats + " seats.";
	}
}
