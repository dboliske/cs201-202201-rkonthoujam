package exams.second;

public class ComputerLab extends Classroom {
	private boolean computers;
	
	public ComputerLab() {
		super();
		computers = true;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	public boolean hasComputers() {
		return computers;
	}
	
	public String toString() {
		if (computers)
			return "This is a computers lab classroom which has computers.";
		else
			return "This is a computers lab classroom which doesn't have computers.";
	}
}
