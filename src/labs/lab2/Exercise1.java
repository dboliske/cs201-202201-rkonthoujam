package labs.lab2;

import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) {
		// Prompt the user to input the square size
		Scanner input = new Scanner(System.in);
		System.out.println("Enter square size:");
		int size = Integer.parseInt(input.nextLine());
		for (int i = 0; i < size; i++) { // for loop for each line
			for (int j = 0; j < size; j++) { // for each line we print '*' size times
				System.out.print("* ");
			}
			System.out.println();
		}
	}

}
