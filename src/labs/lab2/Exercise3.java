package labs.lab2;

import java.util.Scanner;

public class Exercise3 {

	public static void main(String[] args) {
		// Prompt the user to select one of the option
		Scanner input = new Scanner(System.in);
		boolean b = true;
		while (b) {
			System.out.println("1. Say Hello");
			System.out.println("2. Addition");
			System.out.println("3. Multiplication");
			System.out.println("4. Exit");
			int option = Integer.parseInt(input.nextLine());
			
			int x, y;
			switch (option) {
				case 1 : // Case 1 the user select to print out Hello
					System.out.println("Hello");
					break;
				case 2: // Case 2 the user input two numbers and addition of those two numbers
					System.out.println("Enter 2 numbers:");
					x = Integer.parseInt(input.nextLine());
					y = Integer.parseInt(input.nextLine());
					System.out.println("The sum of the 2 numers is " + (x+y));
					break;
				case 3: // Case 2 the user input two numbers and multiplication of those two numbers
					System.out.println("Enter 2 numbers:");
					x = Integer.parseInt(input.nextLine());
					y = Integer.parseInt(input.nextLine());
					System.out.println("The sum of the 2 numers is " + (x*y));
					break;
				case 4: //
					b = false; // b becomes false when user enters 4
					break;
			}
		}
	}
}