package labs.lab2;

import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {
		// Prompt the user to input their grades
		Scanner input = new Scanner(System.in);
		System.out.println("Enter your grades (enter -1 to exit):");
		int c = 0;
		double s = 0;
		boolean flag = true;
		do {
			double g = Double.parseDouble(input.nextLine());
			if (g != -1) { // if the user didn't enter -1 we increment the count and update the sum of grades
				s = s + g;
				c++;
			}
			else {
				flag = false;
			}
		} while (flag == true); // flag becomes false when user enters -1
		
		if (c >= 1) {
			System.out.println("Your average : " + s/c);
		}
		else {
			System.out.println("There's no grade.");
		}
		
	}
}
