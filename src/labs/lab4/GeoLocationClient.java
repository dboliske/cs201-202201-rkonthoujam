package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		GeoLocation g1, g2;
		g1 = new GeoLocation();
		g2 = new GeoLocation(42, 69);
		
		System.out.println("Latitude of g1(default): " + g1.getLat());
		System.out.println("Longitude of g1(default): " + g1.getLng());
		System.out.println("Latitude of g2(non-default): " + g2.getLat());
		System.out.println("Longitude of g2(non-default): " + g2.getLng());
		System.out.println("g1: " + g1.toString());
		System.out.println("g2: " + g2.toString());
	}

}
