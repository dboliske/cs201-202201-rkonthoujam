package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		
		Potion potion1, potion2;
		potion1 = new Potion();
		potion2 = new Potion(100,"Raj");
		
		System.out.println("Potion1: " + potion1.toString());
		System.out.println("Potion2: " + potion2.toString());

	}

}
