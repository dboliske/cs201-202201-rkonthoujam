package labs.lab4;

public class Potion {
	private String name;
	private double Strength;
	
	public Potion() {
		setName(" ");
		Strength = 0;
	}
	public Potion(double Strength, String name) {
		setName(name);
		setStrength(Strength);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getStrength() {
		return Strength;
	}

	public void setStrength(double strength) {
		Strength = strength;
	}
	public String toString() {
		return   getName()  ;
	}
	public boolean validStrength( ) {
		if (getStrength() >= 0 && getStrength() <= 10)
			return true;
		else
			return false;
	}
	public boolean equals(Potion p) {
		if (this.getStrength() == p.getStrength() && this.getName() == p.getName()) {
		
			return true;
		}
		else 	{
			return false;
		}
	}
}