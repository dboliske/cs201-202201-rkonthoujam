package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
		PhoneNumber number1, number2;
		number1 = new PhoneNumber();
		number2 = new PhoneNumber("91","522","696969");
		
		System.out.println("number1: " + number1.toString());
		System.out.println("number2: " + number2.toString());
	}

}
