package labs.lab4;

public class GeoLocation {
	private double lat;
	private double lng;
	
	public GeoLocation() {
		lat = 0;
		lng = 0;
	}
	
	public GeoLocation(double lat, double lng) {
		setLat(lat);
		setLng(lng);
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	public void setLat(double newLat) {
		lat = newLat;
	}
	
	public void setLng(double newLng) {
		lng = newLng;
	}
	
	public String toString() {
		return "(" + lat + ", " + lng + ")";
	}
	
	public boolean validLat(double lat) {
		if (lat >= -90 && lat <= 90)
			return true;
		else
			return false;
	}
	
	public boolean validLng(double lng) {
		if (lat >= -180 && lat <= 180)
			return true;
		else
			return false;
	}
	
	public boolean equals(GeoLocation g) {
		if (lat == g.getLat() && lng == g.getLng())
			return true;
		else
			return false;
	}
}
