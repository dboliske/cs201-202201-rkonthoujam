package labs.lab4;

public class PhoneNumber {
	private String countryCode;
	private String areaCode;
	private String number;
	
	public PhoneNumber() {
		setNumber("");
	}
	
	public PhoneNumber(String cCode, String aCode, String number) {
		
		setCountryCode(cCode);
		setAreaCode(aCode);
		setNumber(number);
		
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	public String toString() {
		return "+" + getCountryCode() + " " + getAreaCode() + " " + getNumber() + "";
	}
	
	public boolean validareaCode( ) {
		if (getAreaCode().length() == 3)
			return true;
		else
			return false;
	}
	public boolean validnumber( ) {
		if (getNumber().length() == 7)
			return true;
		else
			return false;
	}
	public boolean equals(PhoneNumber n) {
		if (this.getNumber() == n.getNumber())
			return true;
		else
			return false;
	}
	
}