package labs.lab1;

import java.util.Scanner;

public class Exercise4a {

	public static void main(String[] args) {
		//Test table
		//Input			Output			Expected
		// 32			 0.0			 0.0
		// 40			 4.4			 4.4
		// 69			 20.5			 20.5
		// 80			 26.6			 26.6
		// 100			 37.7			 37.7
		
		// Prompt a user to input temperature in farenheit
		Scanner input = new Scanner(System.in);
		System.out.print("Temperature in farenheit: ");
		double x = Double.parseDouble(input.nextLine());
		// Printing out the temperature from farenheit to celsius
		System.out.println("Temperature in Celsius: " + ((x-32)*5/9));
	}

}
