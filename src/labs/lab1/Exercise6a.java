package labs.lab1;

import java.util.Scanner;

public class Exercise6a {

	public static void main(String[] args) {
		// Prompt a user to input inches
		Scanner input = new Scanner(System.in);
		System.out.print("Inches: ");
		double x = Double.parseDouble(input.nextLine());
		// Printing out the input value of inches
		System.out.println("Inches to centimeters: " + x);

	}

}
