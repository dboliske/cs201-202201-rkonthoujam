package labs.lab1;

import java.util.Scanner;

public class Exercise3 {

	public static void main(String[] args) {
		// prompt user for name
		Scanner input = new Scanner(System.in);
		System.out.print("Your Name: ");
		String name = (input.nextLine());
		input.close();
		// take the first character from name (index is 0)
		char ch = name.charAt(0);
		System.out.println(ch);
		
	}

}
