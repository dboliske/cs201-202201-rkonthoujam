package labs.lab1;

import java.util.Scanner;

public class Exercise4b {

	public static void main(String[] args) {
		//Test table
		//Input			Output			Expected
		// 32			 89.6			 89.6
		// 40			 104.0			 104.0
		// 69			 156.2			 156.2
		// 80			 176.0			 176.0
		// 100			 212.0			 212.0
		
		// Prompt a user to input temperature in celsius
		Scanner input = new Scanner(System.in);
		System.out.print("Temperature in Celsius: ");
		double x = Double.parseDouble(input.nextLine());
		// Printing out the temperature from celsius to farenheit
		System.out.println("Temperature in Farenheit: " + ((x*9/5)+32));

	}

}
