package labs.lab1;

import java.util.Scanner;

public class Exercise2b {

	public static void main(String[] args) {
		// Prompt the user to input your birth year
		Scanner input = new Scanner(System.in);
		System.out.print("Your birth year: ");
		int x = Integer.parseInt(input.nextLine());
		// Multiplying the birth year by 2
		System.out.println("Your birth year multiplied by 2: " + (x*2));
	}

}
