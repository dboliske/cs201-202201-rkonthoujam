package labs.lab1;

import java.util.Scanner;

public class Exercise6b {

	public static void main(String[] args) {
		//Test table
		//Input			Output			Expected
		// 32			 81.28			 81.28
		// 40			 101.6			 101.6
		// 69			 175.26			 175.26
		// 80			 203.2			 203.2
		// 100			 254.0			 254.0
				
		// Prompt a user to input inches
		Scanner input = new Scanner(System.in);
		System.out.print("Inches: ");
		double x = Double.parseDouble(input.nextLine());
		// Printing out inches to centimeters
		System.out.println("Inches to centimeters: " + (x*2.54));
	}

}
