package labs.lab1;

import java.util.Scanner;

public class Exercise5a {

	public static void main(String[] args) {
		// Prompt the user to input length, breadth and height in inches
		Scanner input = new Scanner(System.in);
		System.out.print("Length of a box in inches: ");
		double x = Double.parseDouble(input.nextLine());
		System.out.print("Breadth of a box in inches: ");
		double y =Double.parseDouble(input.nextLine());
		System.out.print("Height of the box in inches: ");
		double z = Double.parseDouble(input.nextLine());
		// Printing out length breadth and height in inches
		System.out.println("Length Breadth and Height: (" + x + ", " + y + ", " +  z + ")");
	}

}
