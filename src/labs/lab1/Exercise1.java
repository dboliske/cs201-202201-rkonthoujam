package labs.lab1;

import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) {
		// Prompt a user for a first name
		Scanner input = new Scanner(System.in);
		System.out.print("Your Name:");
		String value = input.nextLine();
		// Echo the name given in input
		System.out.println("Echo: " + value);
	}

}
