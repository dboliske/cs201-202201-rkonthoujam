package labs.lab1;

import java.util.Scanner;

public class Exercise2d {

	public static void main(String[] args) {
		// Prompt the user to input height in inches
		Scanner input = new Scanner(System.in);
		System.out.print("Your height in inches: ");
		int x = Integer.parseInt(input.nextLine());
		// Printing out the value of height in inches to feet and inches where inches is an integer
		System.out.println("Your height in feet and inches: " + (x/12) + "''" + x % 12 + "'");
	}

}
