package labs.lab1;

import java.util.Scanner;

public class exercise5b {

	public static void main(String[] args) {
		//Test table
		//Input					Output				Expected
		// 32,64,69			   17344.0				  17344.0
		// 11,42,69			   8238.0				  8238.0
		// 48,75,20			   12120.0				  12120.0
		// 7,8,12			   472.0			  	  472.0
		// 6,9,9			   288.0				  288.0
		// Prompt the user to input length, breadth and height in inches
		Scanner input = new Scanner(System.in);
		System.out.print("Length of a box in inches: ");
		double x = Double.parseDouble(input.nextLine());
		System.out.print("Breadth of a box in inches: ");
		double y =Double.parseDouble(input.nextLine());
		System.out.print("Height of the box in inches: ");
		double z = Double.parseDouble(input.nextLine());
		double wood = 2 * (x * y + y * z + z * x);
		// Printing out the amount of wood required
		System.out.println("Amount of wood required to make a box: " + wood);
	}

}
