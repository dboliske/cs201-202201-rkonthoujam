package labs.lab1;

import java.util.Scanner;

public class Exercise2a {

	public static void main(String[] args) {
		// Prompt the user to input your age and father's age
		Scanner input = new Scanner(System.in);
		System.out.print("Your age: ");
		int x = Integer.parseInt(input.nextLine());
		System.out.print("Your fathers age: ");
		int y = Integer.parseInt(input.nextLine());
		// Subtracting father's age by your age
		System.out.println("Your fathers age - Your age: " + (y-x));
	}

}
