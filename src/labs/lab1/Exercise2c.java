package labs.lab1;

import java.util.Scanner;

public class Exercise2c {

	public static void main(String[] args) {
		// Prompt the user to input height in inches
		Scanner input = new Scanner(System.in);
		System.out.print("Your height in inches: ");
		double x = Double.parseDouble(input.nextLine());
		// Printing out the value of height in inches to cms
		System.out.println("Your height in cms: " + (x*2.54));
	}

}
