package labs.lab7;

public class Number3 {
	
	public static double[] sort(double[] nArray) {
		for(int i = 0; i<nArray.length; i++) {
			int min = i;
			for (int j = i+1; j<nArray.length; j++) {
				if (nArray[j]<nArray[min]) {
					min = j;
				}
			}
			
			if (min != i) {
				double temp = nArray[i];
				nArray[i]=nArray[min];
				nArray[min]= temp;
			}
		}
		return nArray;
	}

	public static void main(String[] args) {
		double[] array = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		array = sort(array);
		
		for (double i : array) {
			System.out.print(i + " ");
		}

	}

}