package labs.lab7;

import java.util.Scanner;

public class Number4
{
	
	public static int search(String[] array, String value) {
		int start = 0;
		int pos = -1;
		int end = array.length;

		boolean found = false;
		
		while(!found && start != end) {
			int middle = (start + end) / 2;
			if (array[middle].equalsIgnoreCase(value)) {
				found = true;
				pos = middle;
			} else if (array[middle].compareToIgnoreCase(value) < 0) {
				start = middle + 1;
			} else {
				end = middle;
			}
		}
		return pos;
	}

	public static void main(String[] args) {
		String[] lang = {"c", "html", "java", "python", "ruby", "scala"};
		
		Scanner input = new Scanner(System.in);
		System.out.print("Search : ");
		String value = input.nextLine();
		int index = search(lang, value);
		
		if (index == -1) {
			System.out.println(value + " not found ");
		} else {
			System.out.println(value + " found at index " + index);
		}
		
		input.close();
		
		

	}

}