package labs.lab7;

public class Number1{


	public static int[] bubbleSort(int[] a) {
		boolean done = false;
		do {
			done = true;
			for (int i = 0; i < a.length - 1; i++) {
				if (a[i + 1] < a[i]) {
					int temp = a[i + 1];
					a[i + 1] = a[i];
					a[i] = temp;
					done = false;
				}

			}

		} while (!done);
			
			return(a);

	}

	public static void main(String[] args) {
		int[] array = { 10, 4, 7, 3, 8, 6, 1, 2, 5, 9 };

		bubbleSort(array);

		for (int x : array) {
			System.out.print(x + ",");
		}

	}
}