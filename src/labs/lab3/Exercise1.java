package labs.lab3;
 
import java.io.*;
 
import java.util.*;
 
public class Exercise1 {
    
    public static void main(String args[]) throws FileNotFoundException {
        
        File input = new File(System.getProperty("user.dir")+"/src/labs/lab3/grades.csv");
        
        Scanner input2 = new Scanner(input);
        
        double total = 0.0;
        
        int count = 0;
        
        while(input2.hasNext()) {
        	
            total += Double.parseDouble(input2.nextLine().split(",")[1]);
            
            count ++;
        	
        }
        
        System.out.println("Average is " + total/count);
        
    }
        
        
}
    
