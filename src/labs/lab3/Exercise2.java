package labs.lab3;
 
import java.util.*;
 
import java.io.*;
 
public class Exercise2 {
    
    public static int[] increase(int[] arr) {
        
        int a[] = new int[arr.length+1];
        
        for(int i = 0; i < arr.length; i++) {
            
            a[i] = arr[i];
            
        }
        
        return a;
        
    }
    
    public static void main(String args[]) throws IOException {
        
        try (Scanner input = new Scanner(System.in)) {
        	
			String s = "";
			
			int count = 0;
			
			int arr[] = new int[1];
			
			while(!s.equalsIgnoreCase("Done")) {
			    
			    System.out.println("Enter a value to store");
			    
			    s = input.next();
			    
			    if(!s.equalsIgnoreCase("Done")) {
			        
			        arr[count] = Integer.parseInt(s);
			        
			        arr = increase(arr);
			        
			        count++;
			        
			    }
			    
			}
			
			System.out.println("Enter the file name");
			            
			String file = input.next();
			
			FileWriter file1 = new FileWriter(System.getProperty("user.dir")+"/src/labs/lab3/"+file);
			
			for(int i=0; i<arr.length-1; i++) {
				
				System.out.println(arr[i]);
			    
			    file1.write(arr[i]+" ");
			    
			}
			
			System.out.println("Saved");
			
			file1.close();
			
		} catch (NumberFormatException e) {

			e.printStackTrace();
		}
        
    }
 
}