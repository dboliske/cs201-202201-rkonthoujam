package labs.lab5;

import java.io.File;
import java.util.Scanner;

public class CTAStopApp {

	public static CTAStation[] readFile() {
		CTAStation[] stations = new CTAStation[34];
		
		try {
			File in = new File("src/labs/lab5/CTAStops.csv");
			Scanner input = new Scanner(in);
			input.nextLine();
			int k = 0;
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String attr[] = line.split(",");
				String name = attr[0];
				double lat = Double.parseDouble(attr[1]);
				double lng = Double.parseDouble(attr[2]);
				String location = attr[3];
				String val = attr[4];
				boolean wheelchair;
				if (val.equals("TRUE"))
					wheelchair = true;
				else
					wheelchair = false;
				val = attr[5];
				boolean open;
				if (val.equals("TRUE"))
					open = true;
				else
					open = false;
				
				CTAStation s = new CTAStation(name, lat, lng, location, wheelchair, open);
				stations[k] = s;
				k++;
			}
			input.close();
		} catch (Exception e) {
			System.out.println("Error encountered reading file");
		}
		
		return stations;
	}
	
	public static void displayStationNames(CTAStation[] stations) {
		for (int i = 0; i < stations.length; i++) {
			System.out.println(stations[i].getName());
		}
	}
	
	public static void displayByWheelchair(CTAStation[] stations, Scanner input) {
		System.out.println("Do you need wheelchair access? Enter 'y' or 'n'");
		String choice = input.nextLine();
		while (choice.equals("y") == false && choice.equals("n") == false) {
			System.out.println("I'm sorry, I couldn't get that. Please enter 'y' or 'n'");
			choice = input.nextLine();
		}
		boolean req;
		if (choice.equals("y"))
			req = true;
		else
			req = false;
		
		boolean found = false;
		for (int i = 0; i < stations.length; i++) {
			if (stations[i].hasWheelchair() == req) {
				found = true;
				System.out.println(stations[i].toString());
			}
		}
		
		if (!found)
			System.out.println("No such stations were found");
	}
	
	public static void displayNearest(CTAStation[] stations, Scanner input) {
		System.out.println("Enter a latitude");
		
		double lat, lng;
		lat = Double.parseDouble(input.nextLine());
		
		System.out.println("Enter a longitude");
		lng = Double.parseDouble(input.nextLine());
		
		double minDist = stations[0].calcDistance(lat, lng);
		int idx = 0;
		for (int i = 1; i < stations.length; i++) {
			if (stations[i].calcDistance(lat, lng) < minDist) {
				minDist = stations[i].calcDistance(lat, lng);
				idx = i;
			}
		}
		
		System.out.println(stations[idx].toString());
	}
	
	public static void main(String[] args) {
		CTAStation[] stations;
		stations = readFile();
		
		Scanner input = new Scanner(System.in);
		boolean exit = false;
		while (exit == false) {
			System.out.println("Choose one of the following options:");
			System.out.println("1. Display stations names");
			System.out.println("2. Display stations with/without wheelchair access");
			System.out.println("3. Display nearest station");
			System.out.println("4. Exit");
			
			String option = input.nextLine();
			
			switch (option) {
				case "1":
					displayStationNames(stations);
					break;
				case "2":
					displayByWheelchair(stations, input);
					break;
				case "3":
					displayNearest(stations, input);
					break;
				case "4":
					exit = true;
					break;
				default:
					System.out.println("I'm sorry, I couldn't get that");
			}
		}
	}

}
