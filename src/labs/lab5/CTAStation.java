package labs.lab5;

public class CTAStation extends GeoLocation {
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	CTAStation() {
		super();
		name = "default";
		location = "default";
		wheelchair = false;
		open = false;
	}
	
	CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		super(lat, lng);
		this.name = name;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;
	}
	
	public String getName() {
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	public String toString() {
		String s = "CTA station named " + name + ", located at " + location + ".";
		
		if (wheelchair) {
			s = s + " This station has wheelchair access.";
		} else {
			s = s + " This station doesn't have wheelchair access.";
		}
		
		if (open) {
			s = s + " This station is open.";
		} else {
			s = s + " This station isn't open.";
		}
		
		s = s + " Station coordinates: " + super.toString();
		
		return s;
	}
	
	public boolean equals(CTAStation s) {
		if (!super.equals(s)) {
			return false;
		}
		
		if (name == s.getName() && location == s.getLocation() && wheelchair == s.wheelchair && open == s.open)
			return true;
		else
			return false;
	}
}
