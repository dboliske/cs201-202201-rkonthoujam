# Lab 0

## Total

19/20

## Break Down

* Eclipse "Hello World" program         5/5
* Correct TryVariables.java & run       4/4
* Name and Birthdate program            5/5
* Square
  * Pseudocode                          2/2
  * Correct output matches pseudocode   2/2
* Documentation                         1/2

## Comments
Only 2 out of 4 programs were documented
Also a tip for myself and better organization, comment/document above the line of code instead of the same line so it's not a super lone line