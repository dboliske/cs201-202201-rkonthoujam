package labs.lab6;

import java.util.Scanner;
import java.util.ArrayList;

public class List {

	public static void printMenu() {
		System.out.println("Choose one of the following options:");
		System.out.println("1. Add customer to queue");
		System.out.println("2. Help customer");
		System.out.println("3. Exit");
	}
	
	static public int promptUser(Scanner input) {
		printMenu();
		int option;
		
		option = Integer.parseInt(input.nextLine());
		return option;
	}
	
	static public ArrayList<String> addCustomer(ArrayList<String> q, String customer) {
		q.add(customer);
		return q;
	}
	
	static public String promptCustomer(Scanner input) {
		System.out.println("Enter the name of the customer:");
		String customer = input.nextLine();
		return customer;
	}
	
	static public int helpCustomer(ArrayList<String> q, int top) {
		if (top + 1 > q.size())
			System.out.println("There is no customer to help.");
		else {
			System.out.println("Customer " + q.get(top) + " has been helped.");
			top++;
		}
		return top;
	}
	
	public static void main(String[] args) {
		 Scanner input = new Scanner(System.in);
		 boolean exit = false;
		 ArrayList<String> q = new ArrayList<String>();
		 int top = 0;
		 while (!exit) {
			 int option = promptUser(input);
			 switch (option) {
			 	case 1:
			 		String customer = promptCustomer(input);
			 		addCustomer(q, customer);
			 		break;
			 	case 2:
			 		top = helpCustomer(q, top);
			 		break;
			 	default:
			 		exit = true;
			 		break;
			 }
		 }
	}

}
